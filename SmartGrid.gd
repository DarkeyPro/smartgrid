@tool
class_name SmartGrid extends UI

enum type{Uniform=0,Width=1,Height=2,FixedRows=3,FixedColumns=4}
enum fit{fit_none,fit_x,fit_y,_fit_both}
@export var grid_type:type=type.Uniform
@export var fit_type:fit=fit._fit_both
@export var rows:=1
@export var columns:=1
@export var padding :={top=0,bottom=0,left=0,right=0}
@export var spacing :=5.0
@export var cell_size :=Vector2(5,5)
func _draw():
	var items:=0
	var cell_rect= Vector2(0,0)
	
	if rows<= 0:
		rows =1
		
	if columns <=0:
		columns =1
		
	for i in get_children():
		if i.visible:
			items+=1
			
	if grid_type == type.FixedColumns :
		custom_minimum_size=Vector2()
		size_flags_horizontal=3
		size_flags_vertical=1
		rows = int(ceil(items/float(columns)))
		cell_rect.x = size.x /float(columns)-((((spacing/2)*columns)/float(columns))*2) - (padding.left/float(columns))-(padding.right/float(columns))
		cell_rect.y=cell_rect.x
		if fit_type==fit._fit_both or fit_type==fit.fit_y:
			size.y=rows*(cell_rect.y+padding.top)+(spacing*rows)
		else:
			size.y=rows*(cell_size.y+padding.top)+(spacing*rows)
	elif grid_type == type.FixedRows:
		custom_minimum_size=Vector2()
		size_flags_horizontal=1
		size_flags_vertical=3
		columns = int(ceil(items/float(rows)))
		cell_rect.y = size.y /float(rows) - ((((spacing/2)*rows)/float(rows))*2)-(padding.top/float(rows)) - (padding.bottom/float(rows))
		cell_rect.x=cell_rect.y
		if fit_type==fit._fit_both or fit_type==fit.fit_x:
			size.x= columns*(cell_rect.x+padding.left)+(spacing*columns)
		else:
			size.x= columns*(cell_size.x+padding.left)+(spacing*columns)
	else:
		fit_type=fit._fit_both
		if size.length()<=0 and get_parent().get("size") is Vector2:
			size.x=get_parent().size.x
			size.y=get_parent().size.y
		size_flags_horizontal=3
		size_flags_vertical=3
		var sqr= sqrt(items)
		rows = int(ceil(items/float(columns))) if grid_type == type.Width else int(ceil(sqr))
		columns = int(ceil(items/float(rows))) if grid_type == type.Height else int(ceil(sqr))
		cell_rect.y = size.y /float(rows) - ((spacing/float(rows))*2)-(padding.top/float(rows)) - (padding.bottom/float(rows))-(float(rows)*spacing)*0.1
		cell_rect.x = size.x /float(columns)-((spacing/float(columns))*2) - (padding.left/float(columns))-(padding.right/float(columns))-(float(columns)*spacing)*0.1
		
	match fit_type:
		fit._fit_both:cell_size=cell_rect
		fit.fit_x:cell_size.x=cell_rect.x
		fit.fit_y:cell_size.y=cell_rect.y
		fit.fit_none:cell_size=cell_size
		
	var columns_count:= 0
	var rows_count:=0
	var count :=0
	
	while count < items:
		rows_count= count/columns 
		columns_count = count%columns
		var xpos =(cell_size.x*columns_count) + (spacing*columns_count) +padding.left+(spacing/2)
		var ypos =(cell_size.y*rows_count) + (spacing*rows_count) + padding.top+(spacing/2)
		var i = get_children()[count]
		i.position =Vector2(xpos,ypos)
		i.size =cell_size
		count+=1
	
	custom_minimum_size.y=size.y if grid_type == type.FixedColumns else 0
	custom_minimum_size.x=size.x if grid_type == type.FixedRows else 0

